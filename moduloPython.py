import json
import pymongo
from pymongo import MongoClient
import ast
import subprocess

dataBaseName = "Simulacion"
dataBaseUrl = "localhost"
dataBasePort = 27017
globalDict = {}

def validateAndStartMongoService():
    pwd='sachita'
    cmd="sh /home/andres/Tesis/simulacion/validateMondoService.sh"
    subprocess.call('echo {} | sudo -S {}'.format(pwd, cmd), shell=True)

def convertirFecha(segundosNetlogo):
    days, hours, minutes, seconds = getDiaHoraMin(segundosNetlogo)

    return "Dias: " + str(days) + " -- Hora: " + str(hours) + ":" + str(minutes) + ":" + str(seconds)

def obtenerHora(segundosNetlogo):
    days, hours, minutes, seconds = getDiaHoraMin(segundosNetlogo)

    return hours 

def getDiaHoraMin(segundosNetlogo):
    
    minutes, seconds = divmod(segundosNetlogo, 60)
    hours, minutes = divmod(minutes, 60)
    days, hours = divmod(hours, 24)

    return days, hours, minutes, seconds

def resetDataBase(nombreColeccion):
    clientDataBase = getDataBaseClient()[dataBaseName]
    clientDataBase.drop_collection(nombreColeccion)

def updateDict(key, value):
    globalDict.update({key:value})

def resetDict():
    globalDict.clear()

def insertarDictMongo(nombreColeccion):
    coleccion = getCollection(nombreColeccion)
    coleccion.insert_one(globalDict)

def getDataBaseClient():
    cliente = MongoClient(dataBaseUrl, dataBasePort)

    return cliente

def getCollection(nombreColeccion):
    return getDataBaseClient()[dataBaseName][nombreColeccion]

def insertarPorQuery(id, nombreColeccion, valor, propiedad):
    filtro, actualizacion = {"_id": id}, {"$set": {propiedad: valor}}
    updateOne(filtro, actualizacion, nombreColeccion)

def actualizarLista(id, nombreColeccion, turnoIn, turnoFin, propiedad):
    filtro, actualizacion = {"_id": id}, {"$addToSet": {propiedad: {"inicio": turnoIn, "fin": turnoFin}}}
    updateOne(filtro, actualizacion, nombreColeccion)

def actualizarListaAeronavesControlador(id, nombreColeccion, turnoIn, avionId, propiedad):
    filtro, actualizacion = {"_id": id, "listaTurnos.inicio": turnoIn}, {"$addToSet": {"listaTurnos.$.aviones": {"avionId": avionId}}}
    print(filtro, actualizacion) 
    updateOne(filtro, actualizacion, nombreColeccion)

def updateOne(filtro, actualizacion, nombreColeccion):
    coleccion = getCollection(nombreColeccion)
    coleccion.update_one(filtro, actualizacion)