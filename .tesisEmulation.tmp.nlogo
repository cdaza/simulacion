extensions [
  py ; extension ejecutar python desde netlogo
]

breed [aviones avion]
aviones-own [nearest-neighbor flockmates fabricante ubicacionX ubicacionY identificadorAvion combustible velocidad permisoAterrizaje controladorAsignado almacenado destino]
breed [controladores controlador]
controladores-own [identificadorControlador nombre activo]
globals[listaTurnosAterrizaje listaNombresControladores listaAtributosAviones listaAtributosControladores controladorActivo radio segundo reloj horaDia totalSegundos turnoIn TurnoFin]

to setup
  clear-all
  reset-ticks
  ask patches [ set pcolor 53 ]

  py:setup py:python ; ejemplificar python en py
  (py:run "from moduloPython import *")
  (py:run "validateAndStartMongoService()")
  (py:run "resetDataBase('aeronaves')")
  (py:run "resetDataBase('controladores')")

  set listaAtributosAviones [ "fabricante" "ubicacionX" "ubicacionY" "identificadorAvion" "combustible" "velocidad" "permisoAterrizaje" "controladorAsignado" "almacenado" "destino" ]
  set listaAtributosControladores [ "identificadorControlador" "nombre" "activo" ]
  set totalSegundos 0
  set horaDia 0
  set radio 3

  crear-controladores
  crear-aviones

  set listaTurnosAterrizaje []

  ask n-of 1 patches with [pxcor = 0 and pycor = 0][
    set pcolor red
  ]

  ask n-of 1 patches with [pxcor = 0 and pycor = -3][
    set pcolor yellow
  ]

  if dibujarRadio [dibujarCirculo]

end


to go
  controlador-activo
  mover-agentes
  if ((count aviones) < numeroAviones) [ crear-aviones ]
  if not any? aviones [ crear-aviones ]
  tick
  clock
end

to mover-agentes

  funcionControlador

  ask aviones [
    ;flock
    let colorDestino destino
    face min-one-of patches with [ pcolor = colorDestino ] [ distance myself ]
    let cercaAeropuerto controladores in-radius radio
    ifelse any? cercaAeropuerto [
      ifelse permisoAterrizaje [
        face min-one-of patches with [ pcolor = red ] [ distance myself ]
        forward velocidad * 3 ;velocidentificadorAviond movimiento
      ]
      [ rt 90
       entrar-circuito-espera radio ; 3 es el radio
       set destino red
      ]
    ]
    [forward velocidad] ;velocidentificadorAviond movimiento

    set combustible combustible - 1

    if (pxcor = 0) and (pycor = 0) [
      set listaTurnosAterrizaje remove identificadorAvion listaTurnosAterrizaje
      show identificadorAvion
      show [nombre] of controladorAsignado
      agregarAvionesListaTurnos first [identificadorControlador] of controladorActivo "controladores" turnoIn  "listaTurnos"
      die
    ]

  ]

end

to flock  ;; turtle procedure
  find-flockmates
  if any? flockmates
    [ find-nearest-neighbor
      if distance nearest-neighbor < 2
        [ separate ]
    ]
end

to find-flockmates  ;; turtle procedure
  set flockmates other turtles in-radius 5
end

to find-nearest-neighbor ;; turtle procedure
  set nearest-neighbor min-one-of flockmates [distance myself]
end

;;; SEPARATE

to separate  ;; turtle procedure

end

to funcionControlador
  ask controladores [

    ;set controladorActivo identificadorControlador
    ask aviones in-radius radio [

      set controladorAsignado controladorActivo

      if not member? identificadorAvion listaTurnosAterrizaje [
          set listaTurnosAterrizaje lput identificadorAvion listaTurnosAterrizaje
      ]

      if first listaTurnosAterrizaje = identificadorAvion [
        set color red
        if (precision pycor 1 = -3 and precision pxcor 1 = 0) [
          set permisoAterrizaje true
        ]
      ]
    ]
  ]

end

to dibujarCirculo
  clear-drawing
  create-turtles 1
    [ set color 57
      set size 2 * radio
      set shape "circle"
      stamp
      die ]
end

to entrar-circuito-espera [r]
  fd (pi * r / 180) * (0.8)
  rt 1
end

to crear-aviones

  (py:run "resetDict()")
  let avionesACrear (numeroAviones - (count aviones))

  create-aviones avionesACrear [
    set flockmates no-turtles
    setxy random-xcor random-ycor
    set fabricante "Airbus"
    set identificadorAvion who
    set combustible 1000
    set velocidad precision(0.0123 + random-float 0.0123) 2
    set permisoAterrizaje false
    set shape "airplane"
    set controladorAsignado 0
    set almacenado true
    set ubicacionX xcor
    set ubicacionY ycor
    set color white
    ifelse pycor < 0 [
      set destino yellow
    ]
    [
      set destino red
    ]

    updateDictAeronaves
    (py:run "insertarDictMongo('aeronaves')")
  ]

end

to updateDictAeronaves

  updateDictPython "segundos" totalSegundos
  updateDictPython "_id" who
  insertarAgentePorAtributos listaAtributosAviones

end

to updateDictPython [key value]
  py:set "key" key
  py:set "value" value
  (py:run "updateDict(key, value)")
end

to insertarAgentePorAtributos [listaAtributos]
  foreach listaAtributos [
      atributo ->
      updateDictPython atributo [runresult atributo] of self
  ]
end

to crear-controladores
  (py:run "resetDict()")
  set listaNombresControladores [ "Andres" "Basualdo" "Camilo"]

  foreach listaNombresControladores [
    nombreControlador ->
    create-controladores 1 [
      setxy 0 0
      set identificadorControlador who
      set nombre nombreControlador
      set shape "person"
      set activo false

      updateDictPython "_id" who
      insertarAgentePorAtributos listaAtributosControladores
   ]
    (py:run "insertarDictMongo('controladores')")
 ]

 controlador-activo

end

to controlador-activo

    if controladorActivo = 0 [
      insertarQueryporId first [identificadorControlador] of controladores with [nombre = "Andres"] "controladores" true "activo"
      set turnoIn totalSegundos
      set controladorActivo controladores with [nombre = "Andres"]
    ]

    let nombreControladorActivo first [nombre] of controladorActivo

    if (horaDia > -1 and horaDia < 8) and (nombreControladorActivo != "Andres") [
      establecerControladorActivo "Andres"
    ]

    if (horaDia > 7 and horaDia < 17) and (nombreControladorActivo != "Basualdo" ) [
      establecerControladorActivo "Basualdo"
    ]

    if (horaDia > 16 and (nombreControladorActivo != "Camilo" ) ) [
      establecerControladorActivo "Camilo"
    ]
end

to establecerControladorActivo [nombreActivo]
  set turnoFin totalSegundos
  insertarQueryporId first [identificadorControlador] of controladorActivo "controladores" false "activo"
  crearListaTurnosControladores first [identificadorControlador] of controladorActivo "controladores" turnoIn turnoFin "listaTurnos"
  set controladorActivo controladores with [nombre = nombreActivo]
  insertarQueryporId first [identificadorControlador] of controladorActivo "controladores" true "activo"
  set turnoIn totalSegundos
end

to insertarQueryporId [id nombreColeccion valor propiedad]
     py:set "id" id
     py:set "nombreColeccion" nombreColeccion
     py:set "valor" valor
     py:set "propiedad" propiedad
    (py:run "insertarPorQuery(id, nombreColeccion, valor, propiedad)")
end

to crearListaTurnosControladores [id nombreColeccion valor1 valor2 propiedad]
     py:set "id" id
     py:set "nombreColeccion" nombreColeccion
     py:set "valor1" valor1
     py:set "valor2" valor2
     py:set "propiedad" propiedad
    (py:run "actualizarLista(id, nombreColeccion, valor1, valor2, propiedad)")
end

to agregarAvionesListaTurnos [id nombreColeccion valor1 valor2 propiedad]
     py:set "id" id
     py:set "nombreColeccion" nombreColeccion
     py:set "valor1" valor1
     py:set "valor2" valor2
     py:set "propiedad" propiedad
    (py:run "actualizarLista(id, nombreColeccion, valor1, valor2, propiedad)")
end

to clock
  set totalSegundos ticks * 50
  py:set "segundos" totalSegundos
  (py:run "from moduloPython import *")
  set reloj (py:runresult "convertirFecha(segundos)")
  set horaDia (py:runresult "obtenerHora(segundos)")
end
@#$#@#$#@
GRAPHICS-WINDOW
266
11
1138
884
-1
-1
13.3
1
10
1
1
1
0
0
0
1
-32
32
-32
32
1
1
1
ticks
30.0

BUTTON
71
74
155
107
NIL
setup
NIL
1
T
OBSERVER
NIL
NIL
NIL
NIL
1

BUTTON
71
113
155
146
NIL
go
T
1
T
OBSERVER
NIL
NIL
NIL
NIL
0

SLIDER
51
158
223
191
numeroAviones
numeroAviones
0
20
9.0
1
1
NIL
HORIZONTAL

SWITCH
76
219
211
252
dibujarRadio
dibujarRadio
0
1
-1000

@#$#@#$#@
## WHAT IS IT?

This example shows how to make turtles "walk" from node to node on a network, by following links.

## EXTENDING THE MODEL

Animate the turtles as they move from node to node.

## RELATED MODELS

* Lattice-Walking Turtles Example
* Grid-Walking Turtles Example

<!-- 2007 -->
@#$#@#$#@
default
true
0
Polygon -7500403 true true 150 5 40 250 150 205 260 250

airplane
true
0
Polygon -7500403 true true 150 0 135 15 120 60 120 105 15 165 15 195 120 180 135 240 105 270 120 285 150 270 180 285 210 270 165 240 180 180 285 195 285 165 180 105 180 60 165 15

arrow
true
0
Polygon -7500403 true true 150 0 0 150 105 150 105 293 195 293 195 150 300 150

box
false
0
Polygon -7500403 true true 150 285 285 225 285 75 150 135
Polygon -7500403 true true 150 135 15 75 150 15 285 75
Polygon -7500403 true true 15 75 15 225 150 285 150 135
Line -16777216 false 150 285 150 135
Line -16777216 false 150 135 15 75
Line -16777216 false 150 135 285 75

bug
true
0
Circle -7500403 true true 96 182 108
Circle -7500403 true true 110 127 80
Circle -7500403 true true 110 75 80
Line -7500403 true 150 100 80 30
Line -7500403 true 150 100 220 30

butterfly
true
0
Polygon -7500403 true true 150 165 209 199 225 225 225 255 195 270 165 255 150 240
Polygon -7500403 true true 150 165 89 198 75 225 75 255 105 270 135 255 150 240
Polygon -7500403 true true 139 148 100 105 55 90 25 90 10 105 10 135 25 180 40 195 85 194 139 163
Polygon -7500403 true true 162 150 200 105 245 90 275 90 290 105 290 135 275 180 260 195 215 195 162 165
Polygon -16777216 true false 150 255 135 225 120 150 135 120 150 105 165 120 180 150 165 225
Circle -16777216 true false 135 90 30
Line -16777216 false 150 105 195 60
Line -16777216 false 150 105 105 60

car
false
0
Polygon -7500403 true true 300 180 279 164 261 144 240 135 226 132 213 106 203 84 185 63 159 50 135 50 75 60 0 150 0 165 0 225 300 225 300 180
Circle -16777216 true false 180 180 90
Circle -16777216 true false 30 180 90
Polygon -16777216 true false 162 80 132 78 134 135 209 135 194 105 189 96 180 89
Circle -7500403 true true 47 195 58
Circle -7500403 true true 195 195 58

circle
false
0
Circle -7500403 true true 0 0 300

circle 2
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240

cow
false
0
Polygon -7500403 true true 200 193 197 249 179 249 177 196 166 187 140 189 93 191 78 179 72 211 49 209 48 181 37 149 25 120 25 89 45 72 103 84 179 75 198 76 252 64 272 81 293 103 285 121 255 121 242 118 224 167
Polygon -7500403 true true 73 210 86 251 62 249 48 208
Polygon -7500403 true true 25 114 16 195 9 204 23 213 25 200 39 123

cylinder
false
0
Circle -7500403 true true 0 0 300

dot
false
0
Circle -7500403 true true 90 90 120

face happy
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 255 90 239 62 213 47 191 67 179 90 203 109 218 150 225 192 218 210 203 227 181 251 194 236 217 212 240

face neutral
false
0
Circle -7500403 true true 8 7 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Rectangle -16777216 true false 60 195 240 225

face sad
false
0
Circle -7500403 true true 8 8 285
Circle -16777216 true false 60 75 60
Circle -16777216 true false 180 75 60
Polygon -16777216 true false 150 168 90 184 62 210 47 232 67 244 90 220 109 205 150 198 192 205 210 220 227 242 251 229 236 206 212 183

fish
false
0
Polygon -1 true false 44 131 21 87 15 86 0 120 15 150 0 180 13 214 20 212 45 166
Polygon -1 true false 135 195 119 235 95 218 76 210 46 204 60 165
Polygon -1 true false 75 45 83 77 71 103 86 114 166 78 135 60
Polygon -7500403 true true 30 136 151 77 226 81 280 119 292 146 292 160 287 170 270 195 195 210 151 212 30 166
Circle -16777216 true false 215 106 30

flag
false
0
Rectangle -7500403 true true 60 15 75 300
Polygon -7500403 true true 90 150 270 90 90 30
Line -7500403 true 75 135 90 135
Line -7500403 true 75 45 90 45

flower
false
0
Polygon -10899396 true false 135 120 165 165 180 210 180 240 150 300 165 300 195 240 195 195 165 135
Circle -7500403 true true 85 132 38
Circle -7500403 true true 130 147 38
Circle -7500403 true true 192 85 38
Circle -7500403 true true 85 40 38
Circle -7500403 true true 177 40 38
Circle -7500403 true true 177 132 38
Circle -7500403 true true 70 85 38
Circle -7500403 true true 130 25 38
Circle -7500403 true true 96 51 108
Circle -16777216 true false 113 68 74
Polygon -10899396 true false 189 233 219 188 249 173 279 188 234 218
Polygon -10899396 true false 180 255 150 210 105 210 75 240 135 240

house
false
0
Rectangle -7500403 true true 45 120 255 285
Rectangle -16777216 true false 120 210 180 285
Polygon -7500403 true true 15 120 150 15 285 120
Line -16777216 false 30 120 270 120

leaf
false
0
Polygon -7500403 true true 150 210 135 195 120 210 60 210 30 195 60 180 60 165 15 135 30 120 15 105 40 104 45 90 60 90 90 105 105 120 120 120 105 60 120 60 135 30 150 15 165 30 180 60 195 60 180 120 195 120 210 105 240 90 255 90 263 104 285 105 270 120 285 135 240 165 240 180 270 195 240 210 180 210 165 195
Polygon -7500403 true true 135 195 135 240 120 255 105 255 105 285 135 285 165 240 165 195

line
true
0
Line -7500403 true 150 0 150 300

line half
true
0
Line -7500403 true 150 0 150 150

pentagon
false
0
Polygon -7500403 true true 150 15 15 120 60 285 240 285 285 120

person
false
0
Circle -7500403 true true 110 5 80
Polygon -7500403 true true 105 90 120 195 90 285 105 300 135 300 150 225 165 300 195 300 210 285 180 195 195 90
Rectangle -7500403 true true 127 79 172 94
Polygon -7500403 true true 195 90 240 150 225 180 165 105
Polygon -7500403 true true 105 90 60 150 75 180 135 105

plant
false
0
Rectangle -7500403 true true 135 90 165 300
Polygon -7500403 true true 135 255 90 210 45 195 75 255 135 285
Polygon -7500403 true true 165 255 210 210 255 195 225 255 165 285
Polygon -7500403 true true 135 180 90 135 45 120 75 180 135 210
Polygon -7500403 true true 165 180 165 210 225 180 255 120 210 135
Polygon -7500403 true true 135 105 90 60 45 45 75 105 135 135
Polygon -7500403 true true 165 105 165 135 225 105 255 45 210 60
Polygon -7500403 true true 135 90 120 45 150 15 180 45 165 90

square
false
0
Rectangle -7500403 true true 30 30 270 270

square 2
false
0
Rectangle -7500403 true true 30 30 270 270
Rectangle -16777216 true false 60 60 240 240

star
false
0
Polygon -7500403 true true 151 1 185 108 298 108 207 175 242 282 151 216 59 282 94 175 3 108 116 108

target
false
0
Circle -7500403 true true 0 0 300
Circle -16777216 true false 30 30 240
Circle -7500403 true true 60 60 180
Circle -16777216 true false 90 90 120
Circle -7500403 true true 120 120 60

tree
false
0
Circle -7500403 true true 118 3 94
Rectangle -6459832 true false 120 195 180 300
Circle -7500403 true true 65 21 108
Circle -7500403 true true 116 41 127
Circle -7500403 true true 45 90 120
Circle -7500403 true true 104 74 152

triangle
false
0
Polygon -7500403 true true 150 30 15 255 285 255

triangle 2
false
0
Polygon -7500403 true true 150 30 15 255 285 255
Polygon -16777216 true false 151 99 225 223 75 224

truck
false
0
Rectangle -7500403 true true 4 45 195 187
Polygon -7500403 true true 296 193 296 150 259 134 244 104 208 104 207 194
Rectangle -1 true false 195 60 195 105
Polygon -16777216 true false 238 112 252 141 219 141 218 112
Circle -16777216 true false 234 174 42
Rectangle -7500403 true true 181 185 214 194
Circle -16777216 true false 144 174 42
Circle -16777216 true false 24 174 42
Circle -7500403 false true 24 174 42
Circle -7500403 false true 144 174 42
Circle -7500403 false true 234 174 42

turtle
true
0
Polygon -10899396 true false 215 204 240 233 246 254 228 266 215 252 193 210
Polygon -10899396 true false 195 90 225 75 245 75 260 89 269 108 261 124 240 105 225 105 210 105
Polygon -10899396 true false 105 90 75 75 55 75 40 89 31 108 39 124 60 105 75 105 90 105
Polygon -10899396 true false 132 85 134 64 107 51 108 17 150 2 192 18 192 52 169 65 172 87
Polygon -10899396 true false 85 204 60 233 54 254 72 266 85 252 107 210
Polygon -7500403 true true 119 75 179 75 209 101 224 135 220 225 175 261 128 261 81 224 74 135 88 99

wheel
false
0
Circle -7500403 true true 3 3 294
Circle -16777216 true false 30 30 240
Line -7500403 true 150 285 150 15
Line -7500403 true 15 150 285 150
Circle -7500403 true true 120 120 60
Line -7500403 true 216 40 79 269
Line -7500403 true 40 84 269 221
Line -7500403 true 40 216 269 79
Line -7500403 true 84 40 221 269

x
false
0
Polygon -7500403 true true 270 75 225 30 30 225 75 270
Polygon -7500403 true true 30 75 75 30 270 225 225 270
@#$#@#$#@
NetLogo 6.0.4
@#$#@#$#@
random-seed 2
setup
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
@#$#@#$#@
default
0.0
-0.2 0 0.0 1.0
0.0 1 1.0 0.0
0.2 0 0.0 1.0
link direction
true
0
Line -7500403 true 150 150 90 180
Line -7500403 true 150 150 210 180
@#$#@#$#@
0
@#$#@#$#@
